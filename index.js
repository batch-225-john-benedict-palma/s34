const express = require("express");

const app = express();


const port = 3000;

app.use(express.json());


app.use(express.urlencoded({extended:true}));


app.get("/home", (req, res) => {
	
	// res.send uses the express JS module's method to send a response back to the client.
	res.send("Welcome to the homepage!");
});



let users = [];

app.post("/signup", (req, res) => {

	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){

	users.push(req.body);

		res.send(`Congrats! User ${req.body.username} is in! `);

	} else {

		//if the username and password are not complete an error msg will be sent back
		res.send("Please input BOTH username and password.")
	}

});


app.get('/users', (req, res) => {

	res.json(users);

})


app.delete("/delete-user", (req, res) => {

	let deletedUser = users.shift(req.body);

	res.send(`User ${deletedUser.username} has been deleted!`);
});


app.listen(port, () => console.log(`Server running at port ${port}`))